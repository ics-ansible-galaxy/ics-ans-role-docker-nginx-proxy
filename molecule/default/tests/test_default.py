import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('nginx_proxies')


def test_nginx_is_listening(host):
    # print(host.ansible.get_variables())
    # localip = host.ansible.get_variables()['ansible_default_ipv4']['address']
    assert host.socket("tcp://0.0.0.0:80").is_listening
    assert host.socket("tcp://0.0.0.0:443").is_listening
