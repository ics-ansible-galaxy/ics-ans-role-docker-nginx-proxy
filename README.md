# ics-ans-role-docker-nginx-proxy

Ansible role to deploy an instance of nginx running inside a docker container.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

```yaml
docker_nginx_proxy_image: registry.esss.lu.se/ics-docker/nginx-ldap
docker_nginx_proxy_tag: nginx-1.15.9
docker_nginx_proxy_container_name: nginx-proxy
docker_nginx_proxy_http_port: 80
docker_nginx_proxy_https_port: 443
docker_nginx_proxy_conf_dir: /etc/nginx_proxy

docker_nginx_proxy_workers: 1
docker_nginx_proxy_workers_conn: 1024

docker_nginx_proxy_headers:
  - "Host $http_host"
  - "X-Forwarded-Host $host"
  - "X-Forwarded-Server $host"
  # - "X-Forwarded-For $proxy_add_x_forwarded_for"
  # - "X-Graylog-Server-URL https://$server_name/"

docker_nginx_proxy_pass: http://172.18.0.1:8080

docker_nginx_proxy_env: {}

docker_nginx_proxy_links: []

docker_nginx_proxy_network: nginx_proxy

docker_nginx_proxy_ldap_enable: false
docker_nginx_proxy_ldap_url: "ldaps://ldap.tn.esss.lu.se/ou=Users,dc=esss,dc=lu,dc=se?uid?sub"
docker_nginx_proxy_ldap_binddn: "uid=nginxproxy,ou=Service accounts,dc=esss,dc=lu,dc=se"
docker_nginx_proxy_ldap_binddn_pass: secret
docker_nginx_proxy_ldap_groups:
  - "cn=bastion_users,ou=ICS,ou=Groups,dc=esss,dc=lu,dc=se"
  - "cn=pss_bastion_users,ou=ICS,ou=Groups,dc=esss,dc=lu,dc=se"
docker_nginx_proxy_ldap_users: []
  - 'uid=alessiocurri,ou=Users,dc=esss,dc=lu,dc=se'

docker_nginx_proxy_local_users:
  - name: user1
    password: "apassword"  # Vault me!
  - name: user2
    password: "anotherpassword"  # Vault me!
```

## Authentication
To enable ldap authentication set ```docker_nginx_proxy_ldap_enable: true``` .
This configuration allows any authenticated user to access the resource behind the proxy.
The ```docker_nginx_proxy_ldap_groups``` and ```docker_nginx_proxy_ldap_users``` lists can be used to authorize only the specified groups and users.

The enable local authentication, redefine ```docker_nginx_proxy_local_users```

If no local nor ldap authentication methods are enabled, the proxy will not require any kind of authentication.
If both local and ldap are enabled, any valid user for both subsystems will be allowed to access the resoruce.

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-docker-nginx-proxy
```

## License

BSD 2-clause
